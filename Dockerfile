#FROM openjdk/openjdk-8-rhel8:v1
FROM adoptopenjdk/openjdk8
ADD target/TallerDocker-1.0.0.jar app-demo.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "app-demo.jar"]
